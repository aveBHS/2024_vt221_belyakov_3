#include <memory>
#include <QtWidgets>
#include <QPixmap>
#include <QPoint>
#include <QVBoxLayout>


class Paint : public QWidget
{
private:
    QColor penColor = Qt::black;
    int thickness = 1;

    bool penMode = false;
    bool lineMode = false;
    bool ellipseMode = false;
    bool rectangleMode = false;

    QImage image;
    QPoint lastPoint;
    QPoint startDrawPoint;
    QPoint currentPoint;
    QPoint endDrawingPoint;

public:
    Paint(QWidget *parent = nullptr) : QWidget(parent)
    {
        setWindowTitle("Paint3000");

        QPushButton *colorButton = new QPushButton("Color");
        connect(colorButton, &QPushButton::clicked, this, &Paint::setColor);

        QPushButton *thicknessButton = new QPushButton("Thickness");
        connect(thicknessButton, &QPushButton::clicked, this, &Paint::setThickness);

        QPushButton *ellipseButton = new QPushButton("Elipse");
        connect(ellipseButton, &QPushButton::clicked, this, &Paint::enableElipseMode);

        QPushButton *rectangleButton = new QPushButton("Rectangle");
        connect(rectangleButton, &QPushButton::clicked, this, &Paint::enableRectangleMode);

        QPushButton *lineButton = new QPushButton("Line");
        connect(lineButton, &QPushButton::clicked, this, &Paint::enableLineMode);

        QPushButton *saveButton = new QPushButton("Save");
        connect(saveButton, &QPushButton::clicked, this, &Paint::saveImage);

        QPushButton *openButton = new QPushButton("Open");
        connect(openButton, &QPushButton::clicked, this, &Paint::openImage);

        /////////////////////////////////////////

        QVBoxLayout *buttonsLayout = new QVBoxLayout(this);
        buttonsLayout->setAlignment(Qt::AlignRight);

        QVBoxLayout *configButtonsLayout = new QVBoxLayout();
        configButtonsLayout->setAlignment(Qt::AlignTop);
        buttonsLayout->addLayout(configButtonsLayout);
        configButtonsLayout->addWidget(colorButton);
        configButtonsLayout->addWidget(thicknessButton);

        QVBoxLayout *paintButtonsLayout = new QVBoxLayout();
        paintButtonsLayout->setAlignment(Qt::AlignCenter);
        buttonsLayout->addLayout(paintButtonsLayout);
        paintButtonsLayout->addWidget(ellipseButton);
        paintButtonsLayout->addWidget(rectangleButton);
        paintButtonsLayout->addWidget(lineButton);

        QVBoxLayout *editButtonsLayout = new QVBoxLayout();
        editButtonsLayout->setAlignment(Qt::AlignBottom);
        buttonsLayout->addLayout(editButtonsLayout);
        editButtonsLayout->addWidget(saveButton);
        editButtonsLayout->addWidget(openButton);

        ///////////////////////////////////////

        std::unique_ptr<QImage> image(new QImage(size(), QImage::Format_RGB32));
        image->fill(Qt::white);

    }


protected:

    void enableLineMode() { lineMode = true; }
    void enableRectangleMode() { rectangleMode = true; }
    void enableElipseMode() { ellipseMode = true; }

    ///////////////////////////////////////

    void drawLine(const QPoint &startPoint, const QPoint &endPoint) {
        QPainter painter(&image);
        painter.setPen(QPen(penColor, thickness, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        painter.drawLine(startPoint, endPoint);
        update();
    }

    void drawRectangle(const QPoint &startPoint, const QPoint &endPoint) {
        QPainter painter(&image);
        painter.setPen(QPen(penColor, thickness, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

        int x = qMin(startPoint.x(), endPoint.x());
        int y = qMin(startPoint.y(), endPoint.y());
        int width = abs(startPoint.x() - endPoint.x());
        int height = abs(startPoint.y() - endPoint.y());

        painter.drawRect(x, y, width, height);

        update();
    }

    void drawPanTrace(const QPoint &endPoint)
    {
        QPainter painter(&image);
        painter.setPen(QPen(penColor, thickness, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        painter.drawLine(lastPoint, endPoint);
        update();
    }

    void drawEllipse(const QPoint &center, int radius)
    {
        QPainter painter(&image);
        painter.setPen(QPen(penColor, thickness, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

        painter.drawEllipse(center, radius, radius);

        update();
    }

    ///////////////////////////////////////

    void resizeEvent(QResizeEvent *event) override {
        if (event->oldSize() != QSize(-1, -1)) {
            std::unique_ptr<QImage> newImage(new QImage(size(), QImage::Format_RGB32));
            newImage->fill(Qt::white);

            QPainter painter(newImage.get());
            painter.drawImage(0, 0, image);

            image = *(newImage.get());
            update();
        }
    }

    void mousePressEvent(QMouseEvent *event) override {
        if (event->button() == Qt::LeftButton) {
            if (rectangleMode || lineMode) {
                startDrawPoint = event->pos();
            } else if (ellipseMode) {
                startDrawPoint = event->pos();

                bool success;
                int radius = QInputDialog::getInt(this, tr("Drawing ellipse"), tr("Ellipse radius:"), 10, 1, 200, 1, &success);
                if (success) {
                    drawEllipse(startDrawPoint, radius);
                    ellipseMode = false;
                }
            } else {
                lastPoint = event->pos();
                penMode = true;
            }
        }
    }

    void mouseMoveEvent(QMouseEvent *event) override {
        if ((event->buttons() & Qt::LeftButton) && penMode) {
            drawPanTrace(event->pos());
            lastPoint = event->pos();
        } else if (rectangleMode || lineMode) {
            currentPoint = event->pos();
            update();
        }
    }

    void mouseReleaseEvent(QMouseEvent *event) override {
        if (event->button() == Qt::LeftButton && penMode) {
            drawPanTrace(event->pos());
            penMode = false;
        } else if (rectangleMode) {
            drawRectangle(startDrawPoint, currentPoint);
            rectangleMode = false;
        } else if (lineMode) {
            drawLine(startDrawPoint, currentPoint);
            lineMode = false;
        }
    }

    void paintEvent(QPaintEvent *event) override {
        QPainter painter(this);
        painter.drawImage(0, 0, image);
    }

    ///////////////////////////////////////

    void setColor()
    {
        penColor = QColorDialog::getColor(penColor, this, "Set color");
    }

    void setThickness()
    {
        bool success;
        int newWidth = QInputDialog::getInt(this, tr("Set thickness"), tr("Thickness:"), thickness, 1, 20, 1, &success);
        if (success) thickness = newWidth;
    }

    ///////////////////////////////////////

    void saveImage() {
        QString fileName = QFileDialog::getSaveFileName(
            this, QString::fromUtf8("Save file"), QDir::currentPath(), "Images (*.jpg)"
        );
        if (!fileName.isEmpty() && !fileName.isNull()) image.save(fileName, "JPEG");
    }

    void openImage() {
        QString fileName = QFileDialog::getOpenFileName(
            this, QString::fromUtf8("Open file"), QDir::currentPath(), "Images (*.jpg, *.png)"
        );
        if (!fileName.isEmpty() && !fileName.isNull()) image.load(fileName);
    }
};

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Paint paint;
    paint.show();

    return app.exec();
}
